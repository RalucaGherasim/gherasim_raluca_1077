
const FIRST_NAME = "Gherasim";
const LAST_NAME = "Raluca";
const GRUPA = "1077C";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value>Number.MAX_SAFE_INTEGER|| value< Number.MIN_SAFE_INTEGER)
      return NaN;
         else if(typeof(value)=='number'||typeof(value)=='string')
        return parseInt(value);
        else if(value!==Number(value) || Number.isFinite(value)==false)
        return NaN; 
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}